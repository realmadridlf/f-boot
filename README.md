# f-boot

### 介绍
简单、高效 spring boot 快速开发前后端分离项目

### 使用技术
- spring boot 2.6
- spring security
- mybatis
- redis
- jetcache
- validation
- mapstruct
- disruptor

### 软件架构
- api 接口、实体定义
- common 公共模块（工具类等）
- service 服务实现包含dao
- web rest接口

### 集成前端项目

- <a target="_blank" href="https://gitee.com/realmadridlf/f-vue-admin.git">f-vue-admin</a>

### 安装教程
- 执行script脚本初始化数据库表结构

### 使用说明
可以作为一个基础项目快速使用

