/*
 * Copyright [2021] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f.exception;

import com.f.enums.ResultEnum;

/**
 * 安全
 *
 * @author liuf
 * @date 2021/12/3 15:48
 */
public class SecurityException extends BaseException {

    public static final SecurityException TO_LOGIN = new SecurityException(ResultEnum.UNAUTHORIZED.getCode(), ResultEnum.UNAUTHORIZED.getMsg());
    public static final SecurityException LOGIN_FAIL = new SecurityException(ResultEnum.LOGIN_FAIL.getCode(), ResultEnum.LOGIN_FAIL.getMsg());

    public SecurityException(int code, String message) {
        super(code, message);
    }
}
