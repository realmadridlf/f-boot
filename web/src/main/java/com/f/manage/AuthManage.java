/*
 * Copyright [2021] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f.manage;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.f.base.Result;
import com.f.cache.CacheTemplate;
import com.f.config.ApplicationProperties;
import com.f.constant.Constant;
import com.f.converter.sys.SysUserConverter;
import com.f.dto.TokenDataDto;
import com.f.dto.sys.SysUserDto;
import com.f.entity.sys.SysUser;
import com.f.enums.ResultEnum;
import com.f.manage.api.IAuthManage;
import com.f.service.sys.SysUserService;
import com.f.utils.AesUtils;
import com.f.utils.IdUtils;
import com.f.utils.Json;
import com.f.utils.RsaUtils;
import com.f.utils.ServiceUtils;
import com.f.vo.sys.SysUserVo;
import io.lettuce.core.SetArgs;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 授权管理
 *
 * @author liuf
 * @date 2021/12/6 17:05
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class AuthManage implements IAuthManage {

    private final SysUserService userService;
    private final CacheTemplate cacheTemplate;
    private final ApplicationProperties config;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Result<Void> register(SysUserDto user) {
        log.info("查看用户是否已经注册");
        if (userService.exists(userService.lambdaQuery().eq(SysUser::getName, user.getName()))) {
            return Result.fail(ResultEnum.USER_EXISTS);
        }
        final SysUser sysUser = new SysUser();
        sysUser.setName(user.getName()).setPassword(ServiceUtils.encodePassword(user.getPassword()));
        userService.insert(sysUser);
        return Result.success();
    }

    @Override
    public Result<Void> logout() {
        cacheTemplate.reactiveObject().del(ServiceUtils.getSysUser().getToken()).subscribe();
        return Result.success();
    }

    @Override
    public Result<SysUserVo> login(TokenDataDto tokenDataDto) {
        final String data = tokenDataDto.getData();
        String key = cacheTemplate.sync().get(tokenDataDto.getToken());
        if (key == null) {
            return Result.fail(ResultEnum.AES_KEY_EXPIRE);
        }
        final SysUserDto sysUserDto = Json.parse(AesUtils.decrypt(data, key), SysUserDto.class);
        final SysUser sysUser = userService.getOne(Wrappers.lambdaQuery(SysUser.class).eq(SysUser::getName, sysUserDto.getName())
                .eq(SysUser::getStatus, Constant.USER_STATUS_OK).last(Constant.LIMIT));
        if (sysUser == null) {
            return Result.fail(ResultEnum.LOGIN_FAIL);
        }

        log.info("开始验证密码");
        if (!ServiceUtils.matches(sysUserDto.getPassword(), sysUser.getPassword())) {
            log.info("密码不正确");
            return Result.fail(ResultEnum.LOGIN_FAIL);
        }

        log.info("登录成功");
        final SysUserVo userVo = SysUserConverter.INSTANCE.e2v(sysUser);
        // 生成token
        final String token = IdUtils.uuid();
        userVo.setToken(token);
        userVo.setKey(key);
        cacheTemplate.async().del(tokenDataDto.getToken());
        cacheTemplate.reactiveObject().set(token, userVo, SetArgs.Builder.ex(config.getAuth().getTokenExpire())).subscribe();
        return Result.success(userVo);
    }

    @Override
    public String getKey(String key) {
        final String token = IdUtils.uuid();
        final String uuid = IdUtils.uuid();
        cacheTemplate.reactive().setex(token, 120, uuid).subscribe();
        return RsaUtils.encryptByPublicKey(StringUtils.replace(key, " ", "+"), token + Constant.VERTICAL + uuid);
    }
}
