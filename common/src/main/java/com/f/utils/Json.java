/*
 * Copyright [2021] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * json 工具类
 * 注意 需要初始化jsonMapper，全局唯一线程安全
 *
 * @author liuf
 * @date 2021/12/17 11:15
 */
@Slf4j
public final class Json {

    private static volatile ObjectMapper jsonMapper;

    /**
     * 需要初始化 jsonMapper
     *
     * @param jsonMapper jsonMapper
     */
    public static void setJsonMapper(ObjectMapper jsonMapper) {
        if (Json.jsonMapper != null) {
            return;
        }
        synchronized (Json.class) {
            if (Json.jsonMapper == null && jsonMapper != null) {
                log.info("jsonMapper init");
                Json.jsonMapper = jsonMapper;
            }
        }
    }

    /**
     * 解析json
     *
     * @param json      json字符串
     * @param valueType 类型
     * @param <T>       返回类型
     * @return 对象
     */
    @SneakyThrows
    public static <T> T parse(String json, Class<T> valueType) {
        return jsonMapper.readValue(json, valueType);
    }

    /**
     * 解析json
     *
     * @param json         json字符串
     * @param valueTypeRef 类型
     * @param <T>          返回类型
     * @return 对象
     */
    @SneakyThrows
    public static <T> T parse(String json, TypeReference<T> valueTypeRef) {
        return jsonMapper.readValue(json, valueTypeRef);
    }

    /**
     * 解析json
     *
     * @param json      json字符串
     * @param valueType 类型
     * @param <T>       返回类型
     * @return 对象
     */
    @SneakyThrows
    public static <T> T parse(String json, JavaType valueType) {
        return jsonMapper.readValue(json, valueType);
    }

    /**
     * 对象转json
     *
     * @param value json对象
     * @return json
     */
    @SneakyThrows
    public static String json(Object value) {
        return jsonMapper.writeValueAsString(value);
    }

    /**
     * 对象转json bytes
     *
     * @param value json对象
     * @return json
     */
    @SneakyThrows
    public static byte[] jsonBytes(Object value) {
        return jsonMapper.writeValueAsBytes(value);
    }

    private Json() {
    }
}
