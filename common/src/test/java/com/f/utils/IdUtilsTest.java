/*
 * Copyright [2021] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f.utils;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

class IdUtilsTest {

    @Test
    void init() {
        CommonUtils.concurrencyExecution(3, () -> {
            for (int i = ThreadLocalRandom.current().nextInt(3); i < 3; i++) {
                IdUtils.init(i);
            }
        }, () -> System.out.println("init"));
    }

    @Test
    void id() {
        IdUtils.id();
        CommonUtils.concurrencyExecution(5, () -> {
            for (int i = 0; i < 1000; i++) {
                IdUtils.id();
            }
        }, () -> System.out.println("id:" + IdUtils.id()));
    }

    @Test
    void uuid() {
        CommonUtils.concurrencyExecution(13, () -> {
            for (int i = 0; i < 1000; i++) {
                IdUtils.uuid();
            }
        }, () -> System.out.println("uuid:" + IdUtils.uuid()));
    }
}
