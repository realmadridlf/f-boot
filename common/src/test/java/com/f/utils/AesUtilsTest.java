package com.f.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class AesUtilsTest {

    private final String data = "{\"name\":\"admin\",\"password\":\"123456\"}";
    private final String key = "243341c37ba11722799b4b37d224dda5";

    @Test
    void encrypt() {
        System.out.println(AesUtils.encrypt(data, key));
    }

    @Test
    void decrypt() {
        Assertions.assertEquals(AesUtils.decrypt("7ougRtTzEiX4wMRvHxLD34vtCP5ehTe8fdbdUXRs3ZLPosED8mIXNKwWo2N/YaZE", key), data);
    }
}