/*
 * Copyright [2021] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f.utils;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class RsaUtilsTest {

    private RsaUtils.RsaKeyPair keyPair;
    private String str;

    @BeforeEach
    void setUp() {
        keyPair = RsaUtils.generateKeyPair();
        str = RandomStringUtils.randomAscii(8);
        System.out.println("公钥：" + keyPair.getPublicKey());
        System.out.println("私钥：" + keyPair.getPrivateKey());
        System.out.println("明文:" + str);
    }

    @Test
    void encryptByPrivateKey() {
        // 私钥加密 公钥解密
        Assertions.assertEquals(RsaUtils.decryptByPublicKey(keyPair.getPublicKey(),
                RsaUtils.encryptByPrivateKey(keyPair.getPrivateKey(), str)), str);
    }


    @Test
    void encryptByPublicKey() {
        // 公钥加密 私钥解密
        Assertions.assertEquals(RsaUtils.decryptByPrivateKey(keyPair.getPrivateKey(),
                RsaUtils.encryptByPublicKey(keyPair.getPublicKey(), str)), str);
    }

}