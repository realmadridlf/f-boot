/*
 * Copyright [2021] [liufeng]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.f.utils;

import com.f.base.Result;
import com.f.vo.sys.SysUserVo;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ThreadLocalRandom;

class JsonTest {

    private static final String PARSE = "parse:";
    private final String json = "{\"code\":200,\"msg\":null,\"data\":{\"id\":1,\"name\":\"admin\","
            + "\"code\":\"000001\",\"phone\":\"18999999999\",\"email\":\"1@qq.com\",\"headImage\":\"\","
            + "\"isAdmin\":1,\"token\":\"f90e1f9821a3e670354ae4b354da7e81\"}}";
    private Result<SysUserVo> obj;

    @BeforeEach
    void setUp() throws JsonProcessingException {
        Json.setJsonMapper(new ObjectMapper());
        obj = new ObjectMapper().readValue(json, new TypeReference<Result<SysUserVo>>() {
        });
    }

    @Test
    void setJsonMapper() {
        CommonUtils.concurrencyExecution(30, () -> {
            for (int i = ThreadLocalRandom.current().nextInt(300); i < 300; i++) {
                Json.setJsonMapper(new ObjectMapper());
            }
        }, () -> System.out.println("setJsonMapper"));
    }

    @Test
    void parse() {
        CommonUtils.concurrencyExecution(50, () -> {
            for (int i = 0; i < 1000; i++) {
                Assertions.assertEquals(Json.parse(json, new TypeReference<Result<SysUserVo>>() {
                }).getData(), obj.getData());
            }
        }, () -> System.out.println(PARSE + Json.parse(json, new TypeReference<Result<SysUserVo>>() {
        })));
    }

    @Test
    void json() {
        CommonUtils.concurrencyExecution(50, () -> {
            for (int i = 0; i < 10000; i++) {
                Assertions.assertEquals(Json.json(obj), json);
            }
        }, () -> System.out.println(PARSE + Json.json(obj)));
    }

    @Test
    void jsonBytes() {
        CommonUtils.concurrencyExecution(50, () -> {
            for (int i = 0; i < 10000; i++) {
                Json.jsonBytes(obj);
                Assertions.assertEquals(new String(Json.jsonBytes(obj)), json);
            }
        }, () -> System.out.println(PARSE + Json.json(obj)));
    }
}